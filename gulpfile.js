var gulp = require('gulp');
var typescript = require('gulp-tsc'),
    watch = require('gulp-watch'),
    tslint = require('gulp-tslint'),
    inject = require("gulp-inject"),
    bowerFiles = require('main-bower-files'),
    concat = require('gulp-concat'),
    del = require('del'),
    uglify = require('gulp-uglify');


var config = {
        app: {
            js: [ 'app/components/**/*.js' ],
            ts: [
                'app/components/app/**/*.ts',
                'app/components/controllers/**/*.ts',
                'app/components/directives/**/*.ts',
                'app/components/services/**/*.ts'
            ],
            index: 'app/index.html'
        },
        build: {
            concat: 'all.js',
            js: 'app/build/js',
            jsglob: ['app/build/js/**/*.js']
        },
        typescriptSettings:
        {
            removeComments: false,
            target: 'ES3',
            module: 'commonjs',
            noExternalResolve: false,
            noImplicitAny: false
        }
    };

gulp.task('compile', ['compile-typescript'], function(){

});

gulp.task("compile-typescript", function () {
    return gulp.src(config.app.ts)
        .pipe(tslint())
        .pipe(tslint.report('verbose'))
        .pipe(typescript(config.typescriptSettings)).on("error", function (err) {
            console.log("Error:" + err);
        })
        .pipe(gulp.dest(config.build.js));
});

gulp.task("watch", function() {

    gulp.watch(['app/components/**/*'], function() {
        gulp.start("inject-javascript");
    }).on("error", function (err) {
        console.log("Watch error:" + err);
    });
});

gulp.task("default", ['inject-javascript'], function () {

});

gulp.task('clean-build', function () {
    del('app/build/**/*');
});

gulp.task("release", ['clean-build', 'compile'], function () {
    var jsStream = gulp.src(config.build.jsglob)
        .pipe(concat(config.build.concat))
        .pipe(gulp.dest(config.build.js))
        .pipe(uglify())
        .pipe(gulp.dest(config.build.js));

    gulp.src(config.app.index)
        .pipe(inject(jsStream))
        .pipe(gulp.dest('app'));
});

gulp.task('inject-javascript', ['clean-build', 'compile'], function () {

    var target = gulp.src(config.app.index);
    // It's not necessary to read the files (will speed up things), we're only after their paths:

    var sources = gulp.src(config.build.jsglob, {read: false});

    return target.pipe(inject(sources, {relative: true}))
        .pipe(inject(gulp.src(bowerFiles(), {read: false}), {name: 'bower'}))
        .pipe(gulp.dest('app'));
});