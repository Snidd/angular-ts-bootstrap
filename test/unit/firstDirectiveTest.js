describe("firstDirective", function() {
    var elm, angelm, scope;

    // Initialize the app
    beforeEach(module('app'));

    // Initialize all the templates as javascript so they wont be requested with GET
    beforeEach(module('templates'));

    beforeEach(inject(function($rootScope, $compile) {
        scope = $rootScope.$new();
        elm = angular.element("<div><first-directive></first-directive></div>");
        angelm = $compile(elm)(scope);
        // $digest is necessary to finalize the directive generation
        scope.$digest();
    }));

    it('should produce the text we wanted', function() {
        expect(elm.text()).toEqual("this is our first directive");
    });
});