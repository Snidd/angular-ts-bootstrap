describe('firstController', function() {
    beforeEach(module('app'));

    var $controller;

    beforeEach(inject(function(_$controller_){
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $controller = _$controller_;
    }));

    describe('$scope', function() {
        it('sets the status to correct string', function() {
            var $scope = {};
            var controller = $controller('app.controllers.firstController', { $scope: $scope });
            expect($scope.status).toEqual('Hello Document!');
        });
    });
});