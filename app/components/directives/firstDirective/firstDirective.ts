/// <reference path="../../../_all_types.ts"/>
'use strict';

module app.directives {

    export class firstDirective implements IDirective {
        templateUrl = 'components/directives/firstDirective/template.html';
        restrict = 'E';
        link = ($scope, element: JQuery, attrs: ng.IAttributes) => {
            element.text('this is our first directive');
        };

    }
}

app.registerDirective('firstDirective', []);