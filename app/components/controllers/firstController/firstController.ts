/// <reference path="../../../_all_types.ts"/>

interface FirstControllerScope extends ng.IScope {
    documents:Document[];
    status:string;
}

module app.controllers {
    export class firstController implements IController {
        constructor (private $scope:FirstControllerScope) {
            $scope.status = "Hello Document!";

        }
    }
}

app.registerController('firstController', ['$scope']);