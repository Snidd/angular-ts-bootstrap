# README #

To get this application running, clone it, remove the original remote, add another remote repository, then run:

**npm install**

### What is this repository for? ###

To get a project with Typescript & Angular up and running quickly.

### How do i compile the typescript? ###

All the compilations are set-up with [gulpjs](http://gulpjs.com/) 

The tasks to use are the following:

* **gulp** *(Compiles the typescript & injects all the javascript into index.html)*
* **gulp release** *(Compiles, Concats, Uglifys, and injects the result into index.html)*
* **gulp watch** *(Starts a watcher that compiles modified files into javascript)*